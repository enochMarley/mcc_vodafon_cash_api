<?php
namespace Models\Config;

require_once 'Services.php';

use Models\Services\Services as Services;

class Config {
    public const DATABASE_SERVER_NAME = '';
    
    public const DATABASE_NAME = '';
    
    public const DATABASE_USER_NAME = '';
    
    public const DATABASE_USER_PASSWORD = '';
    
    public const VF_PIN = '';
    
    public const VENDOR_CODE = '';
    
    public const WSDL_FILE_URI = "http://localhost/VC_API/APIGateway.wsdl";
}