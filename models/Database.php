<?php 
namespace Models\Database;

require_once 'Config.php';

use Models\Config\Config as Config;

class Database {
    protected $server_name = Config::DATABASE_SERVER_NAME;
    protected $database_name = Config::DATABASE_NAME;
    protected $database_user = Config::DATABASE_USER_NAME;
    protected $database_password = Config::DATABASE_USER_PASSWORD;
    
    protected static $database_connection;
    
    /**
     * Constructor of the clas
     */
    public function __construct()
    {
        
    }
    
    
    /**
     * Destructor of the function
     */
    public function __destruct()
    {
        
    }
    
    
    /**
     * Method to connect to the database
     */
    public static function connect_database()
    {
        if (static::$database_connection != null) {
            return static::$database_connection;
        }
        
        echo "Database connected";
    }
    
    
    /**
     * Method to perform database queries
     */
    public static function make_query($query)
    {
        $connection = static::connect_database();
    }
}


?>