<?php
namespace Models\Services;

require_once 'Config.php';

use Models\Config\Config as Config;

class Services {
    /**
     * @var string VF_PIN
     */
    protected const VF_PIN = Config::VF_PIN;
    
    
    /**
     * @var string VENDOR_CODE
     */
    protected const VENDOR_CODE = Config::VENDOR_CODE;
    
    
    /**
     * @var string WSDL_FILE_URI
     */
    protected const WSDL_FILE_URI = Config::WSDL_FILE_URI;
    
        
    /**
     * Method to perform credit services
     */
    public static function execute_credit_service($client_phone_number, $token, $credit_amount)
    {
        $soap_client_header_namespace = '';
        $soap_client_header_name = '';
        $soap_client_options = array("trace" => 1);  
        $soap_client = new \SoapClient(self::WSDL_FILE_URI, $soap_client_options);
        $soap_client_header_options = array();
        $soap_client_header = new \SoapHeader($namespace, $name, $soap_client_header_options);
        
        $soap_client_body_options = array(
            "CreditRequest" => array(
                "Token" => $token,
                "ResultURL" => "",
                "VFPin" => self::VF_PIN,
                "PhoneNumber" => $client_phone_number,
                "Amount" => $credit_amount,
                "VendorCode" => self::VENDOR_CODE,
                "TransactionID" => ""
            )
        );
        
        try {
            $response = $soap_client->__call("creditService", $soap_client_body_options, null, $soap_client_header);
            echo $response;
            
        } catch (\SOAPFault $error) {
            echo $error;
        }
    }
    
    
    /**
     * Method to perform payment services
     */
    public static function execute_payment_service()
    {
        
    }
    
    
    /**
     * Method to perform credit service to agents
     */
    public static function execute_credit_agent_service()
    {
        
    }
    
    
    /**
     * Method to perform credit service to customers
     */
    public static function execute_credit_customer_service()
    {
        
    }
    
    
    /**
     * Method to reverse services
     */
    public static function execute_reversal_service()
    {
        
    }
}