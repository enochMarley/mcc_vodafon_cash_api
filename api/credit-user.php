<?php

namespace API\Credit;

require_once '../models/Database.php';
require_once '../models/Services.php';

use Models\Database\Database as Database;
use Models\Services\Services as Services;

Database::connect_database();
Services::execute_credit_service();

